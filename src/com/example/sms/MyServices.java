package com.example.sms;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.widget.Toast;

public class MyServices extends Service {
	
	private static final String TAG = null;
    MediaPlayer player;

	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
    public void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.one_small_step);
        player.setLooping(true); // Set looping
        player.setVolume(100,100);

    }
	
	@Override
	public int onStartCommand (Intent intent, int flags, int startId) {
		//this service will run until we stop it
		player.start();
		Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
        return 1;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		player.stop();
        player.release();
		Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
	}

}
