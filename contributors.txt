

In spite of sharing this file on bit bucket with my classmate Logan,
I haven't received any contribution from him so far, so I ended up
having to do it by my self. 

The SMS app has two classes, one is the main SendSMSActivity, that handles 
the entire app, and the second is the class that I extended Services,
and will run the services in the background.

This app sends SMS text messages and it uses a Service, to automatically 
play a song in the background and also triggers a Toast in the background.

Using the example from the Big Nerd Ranch book, I've added 2 buttons in the
user interface to start and stop the service that is running in the background.

I pushed the app to a Bitbucket repo:

https://mikecamara@bitbucket.org/mikecamara/sms.git

